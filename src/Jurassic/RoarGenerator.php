<?php

namespace Drupal\dino_roar\Jurassic;

class RoarGenerator
{
  protected $useCache;

  public function __construct($useCache) {
    $this->useCache = $useCache;
  }


  public function getRoar($length)
  {
    $cache_key = __FUNCTION__ .':'. $length;
    if ($this->useCache) {
      \Drupal::getContainer()->get('logger.factory')->get('default')->debug('use cache !');
      $kv = \Drupal::getContainer()->get('keyvalue');
      $store = $kv->get('dino_roar');
      if ($store->has($cache_key)) {
        return $store->get($cache_key);
      }
    }

    sleep(2);
    $roar = 'R'.str_repeat('O', $length).'AR!';

    if ($this->useCache) {
      $kv = \Drupal::getContainer()->get('keyvalue');
      $kv->get('dino_roar')->set($cache_key, $roar);
    }

    return $roar;
  }
}