<?php

namespace Drupal\dino_roar\Tests;

use Drupal\dino_roar\Jurassic\RoarGenerator;

/**
 * Test Dino Roar module.
 *
 * @group dino_roar
 */
class DinoRoarTest extends \Drupal\Tests\UnitTestCase {
  public function testRoar() {
    $generator = new RoarGenerator(false);
    $roar = $generator->getRoar(7);
    $this->assertEquals($roar, 'ROOOOOOOAR!');
  }
}